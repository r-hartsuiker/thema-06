#!/usr/bin/env python3

"""
Converts sam file to bam file and sorts the bam file.
"""

__author__ = "Meint van den Berg"
__date__ = "2019-01-07"
__version__ = "1.0"

import os
import argparse


def sam_bam(samfilesdir):
    """
    Converts sam file to bam
    :param samfilesdir:
    :return:
    """
    if not os.path.isdir("bam/"):
        os.system('mkdir bam')

    samfiles = os.listdir(samfilesdir)
    for file in samfiles:
        os.system('samtools view -S -b ' + samfilesdir + '/' + file + ' > bam/' + file[0:-3] + 'bam')

    return 0


def bam_sort():
    """
    Sorts the bam files
    :param :
    :return:
    """
    if not os.path.isdir("sorted_bam/"):
        os.system('mkdir sorted_bam')

    bamfiles = os.listdir("bam/")
    for file in bamfiles:
        os.system('samtools sort bam/' + file + ' -o sorted_bam/' + file[0:-3] + 'sorted.bam')

    return 0

def main():
    """
    Main
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('samfilesdir')
    args = parser.parse_args()

    sam_bam(args.samfilesdir)
    bam_sort()

    return 0

if __name__ == "__main__":
    main()
