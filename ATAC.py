#!/usr/bin/home
import os
from configparser import ConfigParser

# /usr/bin/env python3

"""
pipeline to pipe certain tools to do research on ATAC-seq data


"""
import argparse
from subprocess import check_output

__authors__ = "Milan Roenhorst, Ruben Hartsuiker, Martijn van der Werff, Meint van den Berg"
__status__ = "module"
__version__ = "1.1.1"


class AtacPipeline(object):
    def __init__(self):

        self._config = read_config()

        # self._config is a dict. Access config arguments parameters using basic dict syntax: self._config.get('arg1'))

    # run a fastqc rapport on every fastq file in the given directory
    # step 1
    def fastqcrapport(self):
        """
        This creates a fastqc rapport
        :return:
        """
        print("Creating fastqc rapports.")
        # create output directory for fastQC rapport
        if not os.path.isdir("fastQC_output"):
            os.system('mkdir fastQC_output')

        fastqfilesdir = self._config.get('fastqfilesdir')

        for fastqcfile in sorted(os.listdir(fastqfilesdir)):
            os.system('fastqc -o fastQC_output/ ' + fastqfilesdir + fastqcfile)

        return None

    def create_sam(self):
        """
        Creates sam files of the fastq files
        :return:
        """
        print("Creating sam files.")
        cores = self._config.get('cores')
        fastqfilesdir = self._config.get('fastqfilesdir')
        indexfilesdir = self._config.get('indexfilesdir')

        if not os.path.isdir("sam/"):
            os.system('mkdir sam')
            os.system('chmod 777 sam')

        index_files = os.listdir(indexfilesdir)

        fastqfiles = sorted(os.listdir(fastqfilesdir))

        for i in range(0, len(fastqfiles), 2):
            print('Processing file', int(((i + 2) / 2)))
            os.system('bowtie2 -p ' + cores + ' -x ' + indexfilesdir + index_files[0][0:-6] + ' -1 ' + fastqfilesdir + fastqfiles[i] +
                      ' -2 ' + fastqfilesdir + fastqfiles[i+1] + ' -S sam/' + fastqfiles[i][0:-16] + '.sam')

        return 0

    def sam_bam(self):
        """
        Converts sam file to bam
        :param:
        :return:
        """
        cores = self._config.get('cores')
        print("Converting sam files to bam files.")
        if not os.path.isdir("bam/"):
            os.system('mkdir bam')

        samfiles = os.listdir("sam/")
        for file in samfiles:
            os.system('samtools view  -@ ' + cores + ' -S -b sam/' + file + ' > bam/' + file[0:-3] + 'bam')

        return 0

    def bam_sort(self):
        """
        Sorts the bam files
        :param :
        :return:
        """
        print("Sorting bam files.")
        if not os.path.isdir("sorted_bam/"):
            os.system('mkdir sorted_bam')

        bamfiles = os.listdir("bam/")
        for file in bamfiles:
            os.system('samtools sort bam/' + file + ' -o sorted_bam/' + file[0:-3] + 'sorted.bam')

        return 0

    def index_bam(self):
        """
        Creates an index for the bam files
        :return:
        """
        print("Creating bam file indexes.")
        sorted_bamfiles = os.listdir("sorted_bam/")
        print(sorted_bamfiles)
        for file in sorted_bamfiles:
            os.system('samtools index -b sorted_bam/' + file)

        return 0

    def call_peak(self):
        """
        calls the peaks in the sorted bam files
        :return:
        """
        print("Determining peaks.")
        if not os.path.isdir("peaks"):
            os.system('mkdir peaks')

        sorted_bamfiles = os.listdir("sorted_bam/")
        for file in sorted_bamfiles:
            os.system('macs2 callpeak -t sorted_bam/' + file + ' -n peaks/' + file[0:3] +
                      ' -g 2716965481 -B --trackline --nomodel --extsize 200 --call-summits')

        return 0

    def compare_peaks(self):
        """
        compares the peaks that were found
        :return:
        """
        print("Comparing peaks.")
        if not os.path.isdir("DiffPeaks"):
            os.system('mkdir DiffPeaks')

        peaks = os.listdir("peaks/")
        used_files = []
        for file in peaks:
            if file[-3:] == "bdg":
                used_files.append(file)

        for i in range(0, len(used_files), 4):
            os.system('macs2 bdgdiff --t1 peaks/' + used_files[i+1] + ' --c1 peaks/' + used_files[i] + ' --t2 peaks/'
                      + used_files[i+3] + ' --c2 peaks/' + used_files[i+2] + ' --o-prefix Diffpeaks/ -g 50 -l 75')

        return 0

    def calc_coverage(self):
        """
        Calculates the coverage
        :return:
        """
        print("Calculating coverage.")
        if not os.path.isdir("coverage"):
            os.system('mkdir coverage')

        sorted_bam = os.listdir("sorted_bam/")
        for bam in sorted_bam:
            if bam[-3:] == "bam":
                os.system('bamCoverage -b sorted_bam/' + bam + ' -o coverage/' + bam[0:3] + '_coverage.bw')

        return 0

    def summary_bam(self):
        """
        Summarize the coverage file
        :return:
        """
        print("Summarizing coverage files.")
        if not os.path.isdir("summary"):
            os.system('mkdir summary')

        coverages = os.listdir("coverage/")
        os.system('multiBigwigSummary bins -b coverage/' + coverages[0] + ' coverage/' + coverages[1] +
                  ' coverage/' + coverages[2] + ' coverage/' + coverages[3] + ' -o summary/CTX_results.npz')
        os.system('multiBigwigSummary bins -b coverage/' + coverages[4] + ' coverage/' + coverages[5] +
                  'coverage/' + coverages[6] + ' coverage/' + coverages[7] + ' -o summary/Hippo_results.npz')
        os.system('multiBigwigSummary bins -b coverage/' + coverages[8] + ' coverage/' + coverages[9] +
                  ' coverage/' + coverages[10] + ' coverage/' + coverages[11] + ' -o summary/CBM_results.npz')
        os.system('multiBigwigSummary bins -b coverage/' + coverages[12] + ' coverage/' + coverages[13] +
                  'coverage/' + coverages[14] + ' coverage/' + coverages[15] + ' -o summary/STR_results.npz')

        return 0

    def create_pca(self):
        """
        Creates the pca plot
        :return:
        """
        print("Creating pca plots.")
        if not os.path.isdir("PCA"):
            os.system('mkdir PCA')

        summaries = os.listdir("summary/")
        for summary in summaries:
            os.system('plotPCA -in summary/' + summary + '-o ' + summary.split("_")[0] + '_PCA.png')

        return 0






def read_config(filename='config.ini', section='atac'):

    # create parser and read ini configuration file
    parser = ConfigParser()
    parser.read(filename)

    db = {}
    if parser.has_section(section):
        items = parser.items(section)
        for item in items:
            db[item[0]] = item[1]
    else:
        raise Exception('{0} not found in the {1} file'.format(section, filename))

    print(db)
    return db


def main():
    # parser = argparse.ArgumentParser()
    # parser.add_argument('-fq', '--FastQCfiles',
    #                     type=str,
    #                     required=True,
    #                     help="Run fastQC rapport. provide path with fastqc files")
    #
    # args = parser.parse_args()

    c1 = AtacPipeline()
    c1.fastqcrapport()
    c1.create_sam()
    c1.sam_bam()
    c1.bam_sort()
    c1.index_bam()
    c1.call_peak()
    c1.compare_peaks()
    c1.calc_coverage()
    c1.summary_bam()
    c1.create_pca()

if __name__ == "__main__":
    main()
