#!/usr/bin/env python3

"""
Module for parsing a fastq file. extracting the DNA sequence from the file
and returning the score.
"""

__author__ = "Martijn van der Werff"
__status__ = "Module"
__version__ = "1.1.1"

import os

def yes_no():

    yes = {'yes', 'y', 'ye', ''}
    no = {'no', 'n'}

    choice = input().lower()
    if choice in no:
        create_dir()
    elif choice in yes:
        print("You can continue")
    else:
        print(list(yes), list(no))
        print("Please select one of the options above")

def create_dir():

    os.mkdir("index_files")

    index_files = os.system("bowtie2-build {} index_files/{}".format(input("enter your input file: "),
                                                                     input("enter your output file: ")))

    return index_files

def main():

    #parser = argparse.ArgumentParser(description='Genome file parser')
    #parser.add_argument('-f', '--confirm', type=str, required=True, dest="confirmation")
    #parser.add_argument('-o', '--output_file', type=str, required=True, dest="output_file")
    #args = parser.parse_args()
    print("To continue you need a index file, do you have this?\n")
    yes_no()
    #question = "To continue you need an index file, do you have this?"
    #answer = input("Enter yes or no: ")
    #if answer == "yes":
    #    print("You don't need anything else, you can continue")
    #if answer == "no":
    #    create_index(args.input_files)
    #    print("Run IndexTrace.py for an index file")

if __name__ == "__main__":
    main()