#!/usr/bin/env python3

"""
Docsting

    Usage:
        ~/python3 create_sam.py <index file pathway> <fastq files pathway>
"""

##METADATA VARIABLES
__author__ = "Ruben Hartsuiker"
__data__ = "2018-12-07"
__version__ = "1.0"

##CONSTANTS


##IMPORTS
import os
import sys
import argparse

##FUNCTIONS
def create_sam(indexfiledir, fastqfilesdir):

    if not os.path.isdir("sam/"):
        os.system('mkdir sam')

    index_files = os.listdir(indexfiledir)

    fastqfiles = os.listdir(fastqfilesdir)

    for i in range(len(fastqfiles), 2):
        os.system('bowtie2 -x' + indexfiledir + index_files[0][0:-6] + '-1' + fastqfiles[i] + '-2' + fastqfiles[i+1] + '-o sam/' + fastqfiles[i][0:-16] + '.sam')

##MAIN
def main():
    #PREPERATION
    index_pathway = 'data/index_mus_musculus'
    fastq_pathway = '/commons/Themas/Thema06/Themaopdracht/insane_brain/ATAC_microglia/fastqFiles'

    #EXECUTE
    create_sam(index_pathway, fastq_pathway)

    #FINALIZE

    return 0

if __name__ == "__main__":
    sys.exit(main())